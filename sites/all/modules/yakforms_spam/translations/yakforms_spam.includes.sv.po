# $Id$
#
# LANGUAGE translation of Drupal (sites-all-modules-framaforms_spam-includes)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-10-08 16:35+0200\n"
"PO-Revision-Date: 2021-08-12 20:15+0000\n"
"Last-Translator: Filip Bengtsson <filipbengtsson@live.se>\n"
"Language-Team: Swedish <https://weblate.framasoft.org/projects/framaforms/"
"spam-includes/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.6.2\n"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:107;216;396
msgid "Master"
msgstr ""

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:109
msgid "more"
msgstr "mer"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:110;219;399
msgid "Apply"
msgstr "Tillämpa"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:111;220;400
msgid "Reset"
msgstr "Återställ"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:112;221;401
msgid "Order by"
msgstr "Sortera efter"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:113;222;402
msgid "Asc"
msgstr "Stigande"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:114;223;403
msgid "Desc"
msgstr "Fallande"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:115;224;404
msgid "Elements per page"
msgstr "Objekt per sida"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:116;225
msgid "- All -"
msgstr "- Alla -"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:117;226;406
msgid "Offset"
msgstr "Förskjutning"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:118;227;407
msgid "« first"
msgstr "« första"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:119;228;408
msgid "‹ previous"
msgstr "‹ föregående"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:120;229;409
msgid "next ›"
msgstr "nästa ›"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:121;230;410
msgid "last »"
msgstr "sista »"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:122
msgid "Content"
msgstr "Innehåll"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:123;412
msgid "- Choose an operation -"
msgstr "- Välj en åtgärd -"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:124;415
msgid "Nid"
msgstr ""

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:125;413
msgid "Title"
msgstr "Titel"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:126
msgid "uid"
msgstr ""

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:127;236;419
msgid "Page"
msgstr "Sida"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:218;398
msgid "plus"
msgstr "plus"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:232
msgid "[nid]"
msgstr ""

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:235
msgid "[uid]"
msgstr ""

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:397
msgid "Suspicious forms"
msgstr "Misstänkta formulär"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:405
msgid "- Tout -"
msgstr "- Alla -"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:411
msgid "Contenu"
msgstr "Innehåll"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:414
msgid "Publication date"
msgstr "Publiceringsdatum"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:417
msgid "Published"
msgstr "Publicerad"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:418
msgid "Author is not"
msgstr ""
